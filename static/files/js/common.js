$(window).ready(function() {
  // pre-loader script 
  $(".pre-loader").fadeOut("slow");
  
  // active link 
  for (var i = 0; i < document.links.length; i++) {
   if (document.links[i].href == document.URL) {
   $(document.links[i]).addClass('active');
 }}
  
});

// close-modal
$('.close-modal, .pop-up .modal-footer .delete-btn').on('click',function() {
  $('.modal').modal('hide');
});

 // toggle-password
 $(".toggle-password").click(function() {
  //$(this).toggleClass("fa-eye fa-eye-slash");
  var input = $($(this).attr("toggle"));
  if (input.attr("type") == "password") {
    input.attr("type", "text");
  } else {
    input.attr("type", "password");
  }
  });
  
// submitAnyForm
function submitAnyForm(src) 
    {
    var form=$(src).parents('form').first();
    if(! form[0].checkValidity())
        $('<input type="submit">').hide().appendTo(form).click().remove();
    else
        {
        $(src).removeAttr('onclick');
        $(src).prop('onclick',null).off('click');
        $(src).text('Submitting...');
        form.submit();
        }
  }

// Confirm password front-end validation script  
function chkPasswordMatch() {
  var password = $("#comm_confirm_password").val();
  var confirmPassword = $("#comm_new_password").val();
  if (password != confirmPassword){
      $("#comm_password_match").html("Passwords do not match!");
      $("#comm_change_password_btn").attr('disabled',true);
  }
  else{
      $("#comm_password_match").html("Passwords match.");
      $("#comm_change_password_btn").attr('disabled',false);
  }
}
$(document).ready(function () {
    $("#comm_confirm_password, #comm_new_password").keyup(chkPasswordMatch);
});


$(window).ready(function() {
  $('ul.sub-menu li a.active').first().parents('ul').first().addClass('open-sub-menu');
  
  // page-form, pop-up 'col height-set'
  $('.page-col textarea').parents('.page-col').addClass('height-set');
  $('.pop-up-col textarea').parents('.pop-up-col').addClass('height-set');
  
 // page-form, pop-up 'choose-img-div'
  $('.page-col .choose-img-show').parents('.page-col').addClass('choose-img-div');
  $('.pop-up-col .choose-img-show').parents('.pop-up-col').addClass('choose-img-div');
  
});

// date_pick   
$(function () {
 $('.date_pick').datetimepicker({ 
   pickTime: false, 
   format: "YYYY-MM-DD", 
 });
});



$(document).ready(function() {
  var input = $('.clock-pick');
    input.clockpicker({
        autoclose: true
        //twelvehour: true // AM PM
        // https://weareoutman.github.io/clockpicker/
  });
  });
  // clock-pick, .date-pick keyboard false
  $('.clock-pick').on('focus',function(){
      $(this).trigger('blur');
  });
// profile_popup
$('.profile_popup_btn').click(function() {
 $('.profile_popup').toggleClass('profile_popup_open');
});

// jquery-file-upload-preview-remove-image
// https://stackoverflow.com/questions/40609463/jquery-file-upload-preview-remove-image
function readURL() {
 var $input = $(this);
 var $newinput =  $(this).parent().find('.choose_img_show');
 if (this.files && this.files[0]) {
     var reader = new FileReader();
     reader.onload = function (e) {
       $newinput.attr('src', e.target.result).show();
     }
     reader.readAsDataURL(this.files[0]);
 }
}
$(".choose_img").change(readURL);


// front-end table search
!function(t){"use strict";var e=function(e){function n(n){o=n.target;var r=t.getElementsByClassName(o.getAttribute("data-table"));e.forEach.call(r,function(t){e.forEach.call(t.tBodies,function(t){e.forEach.call(t.rows,a)})})}function a(t){var e=t.textContent.toLowerCase(),n=o.value.toLowerCase();t.style.display=-1===e.indexOf(n)?"none":"table-row"}var o;return{init:function(){var a=t.getElementsByClassName("filter");e.forEach.call(a,function(t){t.oninput=n})}}}(Array.prototype);t.addEventListener("readystatechange",function(){"complete"===t.readyState&&e.init()})}(document);