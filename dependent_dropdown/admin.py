from django.contrib import admin
from .models import Student, Country, State, City 



class CountryAdmin(admin.ModelAdmin):
    list_display = ['name']

class CityAdmin(admin.ModelAdmin):
    list_display = ['name','state',] 

class StateAdmin(admin.ModelAdmin):
    list_display = ['name','country'] 

    
admin.site.register(Student)
admin.site.register(Country, CountryAdmin)
admin.site.register(State,StateAdmin)
admin.site.register(City,CityAdmin)

# Register your models here.
