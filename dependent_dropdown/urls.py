
from django.urls import path,include
from django.conf.urls import url
from . import views

urlpatterns = [
    
    url(r'^$',views.home ,name="home"),
    url(r'^load_states/$',views.load_states ,name="load_states"),
    url(r'^load_cities/$',views.load_cities ,name="load_cities"),
    url(r'^photo/$',views.photo_list ,name="photo_list"),


]