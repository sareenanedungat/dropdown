from django.shortcuts import render, redirect
from . models import Student, State, Country, City
from django.http import JsonResponse
from .models import Photo
from .forms import PhotoForm



def home(request):
    print("ijiij")
    obj = Country.objects.all()
    context = {
        'country':obj,
    }
    return render(request,'dependent_dropdown/listview.html',context)

def load_states(request):

    print("Hello")
    country = request.GET.get('country')
    states = tuple(State.objects.filter(country_id=country).values('id','name'))
    
   
    context ={
        'states': states,
    }
    
    return JsonResponse(context)


def load_cities(request):
    print("insde cities")
    state = request.GET.get('state_id')
    cities = tuple(City.objects.filter(state_id=state).values('id','name'))
    context = {
        'cities': cities,
    }
    print(state)
    return JsonResponse(context)

def photo_list(request):
    photos = Photo.objects.all()
    if request.method == 'POST':
        form = PhotoForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            return redirect('photo_list')
    else:
        form = PhotoForm()
    return render(request, 'dependent_dropdown/photo.html', {'form': form, 'photos': photos})    


  


