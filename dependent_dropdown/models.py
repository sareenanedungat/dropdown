from django.db import models

class Country(models.Model):
    name = models.CharField(max_length=30)

class State(models.Model):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    name = models.CharField(max_length=30)

class City(models.Model):
     
    state = models.ForeignKey(State, on_delete=models.CASCADE) 
    name = models.CharField(max_length=30)

class Photo(models.Model)  :
    file = models.ImageField()
    description = models.CharField(max_length=255, blank=True)
    uploaded_at = models.DateTimeField(auto_now_add=True)



class Student(models.Model):
    name = models.CharField(max_length=100)
    age = models.IntegerField(null=True)
    country = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True)
    state = models.ForeignKey(State, on_delete=models.SET_NULL, null=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True)
    photo = models.ForeignKey(Photo, on_delete=models.SET_NULL, null=True)
