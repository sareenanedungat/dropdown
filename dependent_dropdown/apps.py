from django.apps import AppConfig


class DependentDropdownConfig(AppConfig):
    name = 'dependent_dropdown'
